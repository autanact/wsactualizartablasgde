
/**
 * WSActualizarTablasGDEMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */
        package co.net.une.www.svc;

        /**
        *  WSActualizarTablasGDEMessageReceiverInOut message receiver
        */

        public class WSActualizarTablasGDEMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        WSActualizarTablasGDESkeleton skel = (WSActualizarTablasGDESkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJava(op.getName().getLocalPart())) != null)){

        

            if("actualizarTablasGDE".equals(methodName)){
                
                co.net.une.www.gis.WSActualizarTablasGDERS wSActualizarTablasGDERS7 = null;
	                        co.net.une.www.gis.WSActualizarTablasGDERQ wrappedParam =
                                                             (co.net.une.www.gis.WSActualizarTablasGDERQ)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    co.net.une.www.gis.WSActualizarTablasGDERQ.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               wSActualizarTablasGDERS7 =
                                                   
                                                   
                                                           wrapactualizarTablasGDE(
                                                       
                                                        

                                                        
                                                       skel.actualizarTablasGDE(
                                                            
                                                                getNombreTabla(wrappedParam)
                                                            ,
                                                                getNombreDataset(wrappedParam)
                                                            ,
                                                                getNombreCampoClave(wrappedParam)
                                                            ,
                                                                getValorCampoClave(wrappedParam)
                                                            ,
                                                                getNombresCamposAct(wrappedParam)
                                                            ,
                                                                getValoresCamposAct(wrappedParam)
                                                            )
                                                    
                                                         )
                                                     ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), wSActualizarTablasGDERS7, false);
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.gis.WSActualizarTablasGDERQ param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.gis.WSActualizarTablasGDERQ.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.gis.WSActualizarTablasGDERS param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.gis.WSActualizarTablasGDERS.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, co.net.une.www.gis.WSActualizarTablasGDERS param, boolean optimizeContent)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(co.net.une.www.gis.WSActualizarTablasGDERS.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    

                        private java.lang.String getNombreTabla(
                        co.net.une.www.gis.WSActualizarTablasGDERQ wrappedType){
                        
                                return wrappedType.getWSActualizarTablasGDERQ().getNombreTabla();
                            
                        }
                     

                        private java.lang.String getNombreDataset(
                        co.net.une.www.gis.WSActualizarTablasGDERQ wrappedType){
                        
                                return wrappedType.getWSActualizarTablasGDERQ().getNombreDataset();
                            
                        }
                     

                        private java.lang.String getNombreCampoClave(
                        co.net.une.www.gis.WSActualizarTablasGDERQ wrappedType){
                        
                                return wrappedType.getWSActualizarTablasGDERQ().getNombreCampoClave();
                            
                        }
                     

                        private java.lang.String getValorCampoClave(
                        co.net.une.www.gis.WSActualizarTablasGDERQ wrappedType){
                        
                                return wrappedType.getWSActualizarTablasGDERQ().getValorCampoClave();
                            
                        }
                     

                        private java.lang.String getNombresCamposAct(
                        co.net.une.www.gis.WSActualizarTablasGDERQ wrappedType){
                        
                                return wrappedType.getWSActualizarTablasGDERQ().getNombresCamposAct();
                            
                        }
                     

                        private java.lang.String getValoresCamposAct(
                        co.net.une.www.gis.WSActualizarTablasGDERQ wrappedType){
                        
                                return wrappedType.getWSActualizarTablasGDERQ().getValoresCamposAct();
                            
                        }
                     
                        private co.net.une.www.gis.WSActualizarTablasGDERQType getactualizarTablasGDE(
                        co.net.une.www.gis.WSActualizarTablasGDERQ wrappedType){
                            return wrappedType.getWSActualizarTablasGDERQ();
                        }
                        
                        
                    
                         private co.net.une.www.gis.WSActualizarTablasGDERS wrapactualizarTablasGDE(
                            co.net.une.www.gis.WSActualizarTablasGDERSType innerType){
                                co.net.une.www.gis.WSActualizarTablasGDERS wrappedElement = new co.net.une.www.gis.WSActualizarTablasGDERS();
                                wrappedElement.setWSActualizarTablasGDERS(innerType);
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (co.net.une.www.gis.WSActualizarTablasGDERQ.class.equals(type)){
                
                           return co.net.une.www.gis.WSActualizarTablasGDERQ.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (co.net.une.www.gis.WSActualizarTablasGDERS.class.equals(type)){
                
                           return co.net.une.www.gis.WSActualizarTablasGDERS.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    