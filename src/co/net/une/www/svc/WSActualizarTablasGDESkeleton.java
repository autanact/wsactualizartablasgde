
/**
 * WSActualizarTablasGDESkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSActualizarTablasGDESkeleton java skeleton for the axisService
     */
    public class WSActualizarTablasGDESkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSActualizarTablasGDELocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param nombreTabla
                                     * @param nombreDataset
                                     * @param nombreCampoClave
                                     * @param valorCampoClave
                                     * @param nombresCamposAct
                                     * @param valoresCamposAct
         */
        

                 public co.net.une.www.gis.WSActualizarTablasGDERSType actualizarTablasGDE
                  (
                  java.lang.String nombreTabla,java.lang.String nombreDataset,java.lang.String nombreCampoClave,java.lang.String valorCampoClave,java.lang.String nombresCamposAct,java.lang.String valoresCamposAct
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("nombreTabla",nombreTabla);params.put("nombreDataset",nombreDataset);params.put("nombreCampoClave",nombreCampoClave);params.put("valorCampoClave",valorCampoClave);params.put("nombresCamposAct",nombresCamposAct);params.put("valoresCamposAct",valoresCamposAct);
		try{
		
			return (co.net.une.www.gis.WSActualizarTablasGDERSType)
			this.makeStructuredRequest(serviceName, "actualizarTablasGDE", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    